﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionFacturas : Form
    {
        private DataSet dsFactura;
        private BindingSource bsFactura;

        public FrmGestionFacturas()
        {
            InitializeComponent();
            bsFactura = new BindingSource();
        }

        public DataSet DsFactura
        {
            get
            {
                return dsFactura;
            }

            set
            {
                dsFactura = value;
            }
        }

        private void FrmGestionFacturas_Load(object sender, EventArgs e)
        {
            // ComboBox
            cmbFilter.DisplayMember = "NA";
            cmbFilter.ValueMember = "Id";
            cmbFilter.DataSource = dsFactura.Tables["Empleado"];

            // Tabla
            bsFactura.DataSource = dsFactura;
            bsFactura.DataMember = dsFactura.Tables["Factura"].TableName;
            dgvFacturas.DataSource = bsFactura;
            dgvFacturas.AutoGenerateColumns = true;
        }

        private void chkClientes_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void cmbFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            bsFactura.Filter = string.Format("Empleado = {0}", cmbFilter.SelectedValue);
        }

        private void btnVer_Click(object sender, EventArgs e)
        {
            FrmReporteFacturasRealizadas frfr = new FrmReporteFacturasRealizadas();
            frfr.DsFactura = dsFactura;
            frfr.DrEmpleado = ((DataRowView)cmbFilter.SelectedItem).Row;
            frfr.Show();
        }
    }
}
