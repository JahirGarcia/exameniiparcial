﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmReporteFacturasRealizadas : Form
    {
        private DataSet dsFactura;
        private DataRow drEmpleado;

        public FrmReporteFacturasRealizadas()
        {
            InitializeComponent();
        }

        public DataSet DsFactura
        {
            get
            {
                return dsFactura;
            }

            set
            {
                dsFactura = value;
            }
        }

        public DataRow DrEmpleado
        {
            get
            {
                return drEmpleado;
            }

            set
            {
                drEmpleado = value;
            }
        }

        private void FrmReporteFacturasRealizadas_Load(object sender, EventArgs e)
        {
            DataTable dtFactura = dsFactura.Tables["Factura"];

            DataRow[] drFacturasEmpleado =
                dtFactura.Select(String.Format("Empleado = {0}",
                drEmpleado["Id"]));

            DataTable dtFacturasEmpleado = dtFactura.Clone();

            foreach(DataRow dr in drFacturasEmpleado)
            {
                DataRow drNew = dtFacturasEmpleado.NewRow();
                drNew["CodFactura"] = dr["CodFactura"];
                drNew["Fecha"] = dr["Fecha"];
                drNew["Observaciones"] = dr["Observaciones"];
                drNew["Empleado"] = dr["Empleado"];
                drNew["SubTotal"] = dr["SubTotal"];
                drNew["Iva"] = dr["Iva"];
                drNew["Total"] = dr["Total"];

                dtFacturasEmpleado.Rows.Add(drNew);
            }

            this.reportViewer1.LocalReport.ReportEmbeddedResource = "NETProjectTutorial.ReporteFacturasRealizadas.rdlc";
            ReportDataSource rds1 = new ReportDataSource("dsFacturasEmpleado", dtFacturasEmpleado);
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(rds1);

            this.reportViewer1.RefreshReport();
        }
    }
}
