﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class Factura
    {
        private int id;
        private string codFactura;
        private DateTime fecha;
        private Empleado empleado;
        private string observaciones;
        private double subtotal;
        private double iva;
        private double total;

        public Factura(int id, string codFactura, DateTime fecha, Empleado empleado, string observaciones, double subtotal, double iva, double total)
        {
            this.id = id;
            this.codFactura = codFactura;
            this.fecha = fecha;
            this.empleado = empleado;
            this.observaciones = observaciones;
            this.subtotal = subtotal;
            this.iva = iva;
            this.total = total;
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string CodFactura
        {
            get
            {
                return codFactura;
            }

            set
            {
                codFactura = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        internal Empleado Empleado
        {
            get
            {
                return empleado;
            }

            set
            {
                empleado = value;
            }
        }

        public string Observaciones
        {
            get
            {
                return observaciones;
            }

            set
            {
                observaciones = value;
            }
        }

        public double Subtotal
        {
            get
            {
                return subtotal;
            }

            set
            {
                subtotal = value;
            }
        }

        public double Iva
        {
            get
            {
                return iva;
            }

            set
            {
                iva = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }
    }
}
